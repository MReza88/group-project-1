import sqlite3

CREATE_TABLE_PURCHASE = """CREATE TABLE IF NOT EXISTS purchase (
    id         INTEGER PRIMARY KEY AUTOINCREMENT,
    product_id         REFERENCES product (id) 
                       NOT NULL,
    user_id            REFERENCES user (id) 
                       NOT NULL,
    amount             NOT NULL,
    timestamp  DATE    NOT NULL
);
"""

CREATE_TABLE_USER = """CREATE TABLE IF NOT EXISTS user (
    id       PRIMARY KEY
             NOT NULL,
    name     NOT NULL,
    balance  NOT NULL
);
"""

CREATE_TABLE_PRODUCT = """CREATE TABLE IF NOT EXISTS product (
    id    INTEGER PRIMARY KEY AUTOINCREMENT
                  NOT NULL,
    name          NOT NULL,
    price         NOT NULL
);
"""


INSERT_USER = "INSERT INTO user (id, name, balance) VALUES (?,?,?)"


# INSERT_INVOICE = "INSERT INTO invoice (customer_id, amount_euro, mark) VALUES (?,?, 'unpaid')"
# UPDATE_INVOICE_PAID = """
# UPDATE invoice
# SET
#     mark = 'paid'
# WHERE id = (?)
# """

GET_ALL_USERS = "SELECT * FROM user;"
GET_ALL_INVOICES = "SELECT * FROM invoice;"
GET_ALL_UNPAID_INVOICES = """
SELECT 
    i.id,
    name,
    amount_euro
FROM invoice i
JOIN customer c ON c.id = i.customer_id
WHERE i.mark = 'unpaid';
"""
GET_STATS = """
SELECT
    COUNT(*),
    SUM(amount_euro),
    (SELECT 
        SUM(amount_euro) 
    FROM invoice i
    JOIN customer c ON c.id = i.customer_id
    WHERE i.mark = 'paid')
FROM invoice i
JOIN customer c ON c.id = i.customer_id
WHERE i.mark = 'unpaid';
"""


def connect_db():
    return sqlite3.connect("lib/db/database.db")

c = connect_db()

def create_tables(c):
    with c:
        c.execute(CREATE_TABLE_PURCHASE)
        c.execute(CREATE_TABLE_PRODUCT)
        c.execute(CREATE_TABLE_USER)


def add_user(c, user_id, name):
    with c:
        c.execute(INSERT_USER, (user_id, name, 0,))


def get_all_customers(c):
    with c:
        return c.execute(GET_ALL_CUSTOMERS).fetchall()


def add_invoice(c, customer_id, amount_euro):
    with c:
        c.execute(INSERT_INVOICE, (customer_id, amount_euro))


def get_all_invoices(c):
    with c:
        return c.execute(GET_ALL_INVOICES).fetchall()


def get_all_unpaid_invoices(c):
    with c:
        return c.execute(GET_ALL_UNPAID_INVOICES).fetchall()


def update_invoice_paid(c, invoice_id):
    with c:
        return c.execute(UPDATE_INVOICE_PAID, (invoice_id,))


def get_unpaid_invoice_count(c):
    with c:
        return c.execute(GET_ALL_UNPAID_INVOICES).fetchall()


def get_stats(c):
    with c:
        return c.execute(GET_STATS).fetchall()


def create_purchase(c):
    with c:
        c.execute(CREATE_TABLE_PURCHASE)

create_purchase(c)