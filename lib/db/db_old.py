from os.path import isfile
import sqlite3

DB_PATH="lib/db/database.db"
BUILD_PATH=".build.sql"

SELECT_USERS = "SELECT * FROM user"

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Exception as e:
        print(e)

    return conn

connection = create_connection(DB_PATH)

def select_user(conn,id):
    cur = conn.cursor()
    cur.execute(SELECT_USERS)
    

def create_user(conn, user):
    sql = ''' INSERT INTO user(id,name)
              VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, user)
    conn.commit()
    return cur.lastrowid

def select_product(conn,id):
  
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM product WHERE id = {id}")

    rows = cur.fetchall()

    for row in rows:
        print(row)

def select_all_products(conn):
  
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM product")

    rows = cur.fetchall()

    for row in rows:
        print(row)

def create_product(conn, product):
    sql = ''' INSERT INTO product(name,price)
              VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, product)
    conn.commit()
    return cur.lastrowid

def select_all_purchase(conn):
  
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM purchase")

    rows = cur.fetchall()

    for row in rows:
        print(row)

def create_purchase(conn, purchase):
    sql = ''' INSERT INTO product(user_id,amount,timestamp,product_id)
              VALUES(?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, purchase)
    conn.commit()
    return cur.lastrowid


def select_all_users(connection):
    with connection:
        connection.execute(SELECT_USERS)

select_all_users(conn)

#  Testing functions
