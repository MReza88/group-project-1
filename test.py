import sqlite3
from database import INSERT_USER, add_user

sqlite3.connect("lib/db/database.db")

def connect_db():
    return sqlite3.connect("lib/db/database.db")

c = connect_db()

def add_user(c, user_id, name):
    with c:
        c.execute(INSERT_USER, (user_id, name, 0,))

add_user(c, 1, "test")