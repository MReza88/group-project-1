import discord
import os
import time
import random
from discord.ext import commands
from discord import app_commands
from datetime import datetime
from discord.ui import View, Button
import traceback
from database import *
import sqlite3


BOT_OWNER = 644166476668076032

client = commands.Bot(command_prefix=".", case_insensitive = True, intents=discord.Intents.all(), help_command=None)

products = []
def import_products():
    with open("products.txt", "r") as f:
        data = f.readline()
    return data.split(";")

# c = connect_db()

sqlite3.connect("lib/db/database.db").execute(INSERT_USER, ("311943022466170882", "Henk", 0,))


products = import_products()
product_choices = []

def create_products(products, product_choices):
    x = 0
    for product in products:
        if product not in product_choices:
            product_choices.append(discord.app_commands.Choice(name=product, value=x))
            x += 1
    return product_choices

create_products(products, product_choices)


@client.tree.command(name="add_product")
@app_commands.describe(name="Name of the product")
async def add_product(interaction: discord.Interaction, name: str):
    await interaction.response.send_message(f"Product {name} added.") 
    with open("products.txt", "a") as f:
        f.write(f";{name}")
    
    products = import_products()
    product_choices = create_products(products, [])
    define_buy_product_route(product_choices)
    await client.tree.sync()



@client.tree.command(name="remove_product")
@app_commands.describe(name="Name of the product")
async def remove_product(interaction: discord.Interaction, name: str):
    try:
        products = import_products()
        
        if name in products:
            fin = open("products.txt", "rt+")

            for line in fin:
                pass
                # fin.write(line.replace(f"{name};", ""))   
            fin.close()
                        
            product_choices = create_products(products, [])
            define_buy_product_route(product_choices)
            await interaction.response.send_message(f"Product {name} removed.")
            # await client.tree.sync()
        else: 
            await interaction.response.send_message(f"Product {name} not found!.", ephemeral=True)
    except:
        # printing stack trace
        traceback.print_exc()



def define_buy_product_route(product_choices):
    client.tree.remove_command("buy_product")
    @client.tree.command(name="buy_product")
    @app_commands.describe(product="The product you want to buy", amount="The amount you want to buy", user="[optional] The person you want to buy for")
    @app_commands.choices(product=product_choices)
    async def buy_product(interaction: discord.Interaction, product: discord.app_commands.Choice[int], amount: int = 1, user: discord.User = None):
        """Buy a product from the store"""
        product = product.name
        command_interaction = interaction
        if user == None:
            user = interaction.user
        else:
            await interaction.response.send_message(f"Sent offer to **__{user}__**")
            view = View()
            accept_button = Button(label="yes",style=discord.ButtonStyle.green)
            deny_button = Button(label="no",style=discord.ButtonStyle.red)
            view.add_item(accept_button)
            view.add_item(deny_button)
            dm_message = await user.send(content= f"**__{interaction.user}__** is trying to buy `{product}(x{amount})` for you. Do you accept?", view=view)
            
            async def accept(interaction):
                time_in_amsterdam = datetime.now()
                timestamp = time_in_amsterdam.strftime("%D %H:%M:%S")
                
                with open("purchases.txt", "a") as f:
                    f.write(f"\n{user.id};{product};{amount};{timestamp}")
                
                await dm_message.edit(content=f"**__{command_interaction.user}'s__** offer of `{product}(x{amount})` accepted.", view=None)
                await command_interaction.user.send(f"**__{interaction.user}__** accepted your offer of `{product}(x{amount})`")
        
            async def deny(interaction):    
                await dm_message.edit(content=f"**__{command_interaction.user}'s__** offer of `{product}(x{amount})` declined.", view=None)
                await command_interaction.user.send(f"**__{interaction.user}__** declined your offer of `{product}(x{amount})`")

                with open("purchases.txt", "a") as f:
                    time_in_amsterdam = datetime.now()
                    timestamp = time_in_amsterdam.strftime("%D %H:%M:%S")
                    f.write(f"\n{command_interaction.user.id};{product};{amount};{timestamp}")

            accept_button.callback = accept
            deny_button.callback = deny

        if user == command_interaction.user:
            time_in_amsterdam = datetime.now()
            timestamp = time_in_amsterdam.strftime("%D %H:%M:%S")
            if(amount < 0):
                return await interaction.response.send_message(f"Selected amount `{amount}` As invalid, please enter a valid amount!", delete_after=3, ephemeral=True)
            await interaction.response.send_message(f"Bought {product} `x{amount}`  ", ephemeral = True)
            


define_buy_product_route(product_choices)


@client.tree.command(name="list_purchases")
@app_commands.describe(user="The user you want to view the purchases of")
async def list_purchases(interaction: discord.Interaction, user: discord.User = None):
    """Lists a user's purchases"""
    if user == None:
        user = interaction.user
    embed = discord.Embed(title=f"**{user.name}'s** purchases:")
    with open("purchases.txt", "r") as f:
        header = f.readline()
        header = header.split(";")
        user_purchases = []
        
        data = f.readlines()
        for line in data:
            linelist = line.split(";")
            if int(linelist[0]) == user.id:
                user_purchases.append(line)

    for line in user_purchases:
        line = line.split(";")
        embed.add_field(name=f"**{line[1]} (x{line[2]})**", value=f"`Time: {line[3]}`", inline=False)

    await interaction.response.send_message(embed=embed)


@client.tree.command(name="list_totals")
@app_commands.describe(user="The user you want to view the total purchases of")
async def list_totals(interaction: discord.Interaction, user: discord.User = None):
    """Lists a user's totals"""
    if user == None:
        user = interaction.user
    embed = discord.Embed(title=f"**{user.name}'s** totals:")
    with open("purchases.txt", "r") as f:
        items = {}
        header = f.readline()
        header = header.split(";")
        user_purchases = []
        
        data = f.readlines()


        for line in data:
            linelist = line.split(";")
            if int(linelist[0]) == user.id:
                product = linelist[1]
                amount = int(linelist[2])
                if not product in items.keys():
                    items[product] = amount 
                else:
                    items[product] = items[product] + amount

    for key, value in items.items():
        embed.add_field(name=f"**{key}**", value=f"`{value}`", inline=False)

    await interaction.response.send_message(embed=embed)

# @client.tree.error 
# async def on_app_command_error(interaction, error):
#     await interaction.response.send_message(error, ephemeral=True)


@discord.ui.button(label="yes",style=discord.ButtonStyle.green)
async def accept(button: discord.ui.Button, interaction: discord.Interaction):
    interaction.user.send("This button is working!")


@client.event
async def on_ready():
    try:
        synced = await client.tree.sync()
        print(f"Synced {len(synced)} command(s)")
    except Exception as e:
        print(e)


client.run("MTAyNjc0MTc1NDQyNDIxNzYzMA.GqtBq6.3J7dBReakr_cRourPjZ-YCb-y0EBUjLyr4OeG8")